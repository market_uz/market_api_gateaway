package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateSalePayment godoc
// @ID create_sale_payment
// @Router /sale-payment [POST]
// @Summary Create SalePayment
// @Description  Create SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateSalePayment true "CreateSalePaymentRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "GetSalePaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSalePayment(c *gin.Context) {
	var salePayment kassa_service.CreateSalePayment

	err := c.ShouldBindJSON(&salePayment)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().Create(
		c.Request.Context(),
		&salePayment,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSalePaymentByID godoc
// @ID get_sale_payment_by_id
// @Router /sale-payment/{id} [GET]
// @Summary Get SalePayment By ID
// @Description Get SalePayment By ID
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "SalePaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePaymentByID(c *gin.Context) {

	salePaymentId := c.Param("id")

	if !util.IsValidUUID(salePaymentId) {
		h.handleResponse(c, http.InvalidArgument, "sale payment id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePaymentService().GetByID(
		context.Background(),
		&kassa_service.SalePaymentPrimaryKey{
			Id: salePaymentId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetSalePaymentList godoc
// @ID get_sale_payment_list
// @Router /sale-payment [GET]
// @Summary Get SalePayment List
// @Description Get SalePayment List
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListSalePaymentResponse} "GetAllSalePaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePaymentList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().GetList(
		context.Background(),
		&kassa_service.GetListSalePaymentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSalePayment godoc
// @ID update_sale_payment
// @Router /sale-payment/{id} [PUT]
// @Summary Update SalePayment
// @Description Update SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSalePayment true "UpdateSalePaymentRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "SalePayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSalePayment(c *gin.Context) {

	var salePayment kassa_service.UpdateSalePayment

	salePayment.Id = c.Param("id")

	if !util.IsValidUUID(salePayment.Id) {
		h.handleResponse(c, http.InvalidArgument, "sale payment id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&salePayment)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().Update(
		c.Request.Context(),
		&salePayment,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchSalePayment godoc
// @ID patch_sale_payment
// @Router /sale-payment/{id} [PATCH]
// @Summary Patch SalePayment
// @Description Patch SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "SalePayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchSalePayment(c *gin.Context) {

	var updatePatchSalePayment models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchSalePayment)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchSalePayment.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchSalePayment.ID) {
		h.handleResponse(c, http.InvalidArgument, "sale payment id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchSalePayment.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchSalePayment{
			Id:     updatePatchSalePayment.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSalePayment godoc
// @ID delete_sale_payment
// @Router /sale-payment/{id} [DELETE]
// @Summary Delete SalePayment
// @Description Delete SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SalePayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSalePayment(c *gin.Context) {

	salePaymentId := c.Param("id")

	if !util.IsValidUUID(salePaymentId) {
		h.handleResponse(c, http.InvalidArgument, "sale payment id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePaymentService().Delete(
		c.Request.Context(),
		&kassa_service.SalePaymentPrimaryKey{Id: salePaymentId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
