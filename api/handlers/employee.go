package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/organization_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateEmployee godoc
// @ID create_employee
// @Router /employee [POST]
// @Summary Create Employee
// @Description  Create Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateEmployee true "CreateEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Employee} "GetEmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateEmployee(c *gin.Context) {
	var employee organization_service.CreateEmployee

	err := c.ShouldBindJSON(&employee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().Create(
		c.Request.Context(),
		&employee,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetEmployeeByID godoc
// @ID get_employee_by_id
// @Router /employee/{id} [GET]
// @Summary Get Employee By ID
// @Description Get Employee By ID
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Employee} "EmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEmployeeByID(c *gin.Context) {

	employeeId := c.Param("id")

	if !util.IsValidUUID(employeeId) {
		h.handleResponse(c, http.InvalidArgument, "employee id is an invalid uuid")
		return
	}

	resp, err := h.services.EmployeeService().GetByID(
		context.Background(),
		&organization_service.EmployeePrimaryKey{
			Id: employeeId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetEmployeeList godoc
// @ID get_employee_list
// @Router /employee [GET]
// @Summary Get Employee List
// @Description Get Employee List
// @Tags Employee
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListEmployeeResponse} "GetAllEmployeeResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEmployeeList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().GetList(
		context.Background(),
		&organization_service.GetListEmployeeRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_employee
// @Router /employee/{id} [PUT]
// @Summary Update Employee
// @Description Update Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateEmployee true "UpdateEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Employee} "Employee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateEmployee(c *gin.Context) {

	var employee organization_service.UpdateEmployee

	employee.Id = c.Param("id")

	if !util.IsValidUUID(employee.Id) {
		h.handleResponse(c, http.InvalidArgument, "employee id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&employee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().Update(
		c.Request.Context(),
		&employee,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchEmployee godoc
// @ID patch_employee
// @Router /employee/{id} [PATCH]
// @Summary Patch Employee
// @Description Patch Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Employee} "Employee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchEmployee(c *gin.Context) {

	var updatePatchEmployee models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchEmployee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchEmployee.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchEmployee.ID) {
		h.handleResponse(c, http.InvalidArgument, "employee id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchEmployee.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.EmployeeService().UpdatePatch(
		c.Request.Context(),
		&organization_service.UpdatePatchEmployee{
			Id:     updatePatchEmployee.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteEmployee godoc
// @ID delete_employee
// @Router /employee/{id} [DELETE]
// @Summary Delete Employee
// @Description Delete Employee
// @Tags Employee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Employee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteEmployee(c *gin.Context) {

	employeeId := c.Param("id")

	if !util.IsValidUUID(employeeId) {
		h.handleResponse(c, http.InvalidArgument, "employee id is an invalid uuid")
		return
	}

	resp, err := h.services.EmployeeService().Delete(
		c.Request.Context(),
		&organization_service.EmployeePrimaryKey{Id: employeeId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
