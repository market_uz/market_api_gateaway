package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/tovar_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateProduct godoc
// @ID create_product
// @Router /product [POST]
// @Summary Create Product
// @Description  Create Product
// @Tags Product
// @Accept json
// @Produce json
// @Param profile body tovar_service.CreateProduct true "CreateProductRequestBody"
// @Success 200 {object} http.Response{data=tovar_service.Product} "GetProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProduct(c *gin.Context) {
	var product tovar_service.CreateProduct

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Create(
		c.Request.Context(),
		&product,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetProductByID godoc
// @ID get_product_by_id
// @Router /product/{id} [GET]
// @Summary Get Product By ID
// @Description Get Product By ID
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=tovar_service.Product} "ProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductByID(c *gin.Context) {

	productId := c.Param("id")

	if !util.IsValidUUID(productId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().GetByID(
		context.Background(),
		&tovar_service.ProductPrimaryKey{
			Id: productId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetProductList godoc
// @ID get_product_list
// @Router /product [GET]
// @Summary Get Product List
// @Description Get Product List
// @Tags Product
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=tovar_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&tovar_service.GetListProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateProduct godoc
// @ID update_product
// @Router /product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body tovar_service.UpdateProduct true "UpdateProductRequestBody"
// @Success 200 {object} http.Response{data=tovar_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProduct(c *gin.Context) {

	var product tovar_service.UpdateProduct

	product.Id = c.Param("id")

	if !util.IsValidUUID(product.Id) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Update(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchProduct godoc
// @ID patch_product
// @Router /product/{id} [PATCH]
// @Summary Patch Product
// @Description Patch Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=tovar_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchProduct(c *gin.Context) {

	var updatePatchProduct models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchProduct.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchProduct.ID) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchProduct.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().UpdatePatch(
		c.Request.Context(),
		&tovar_service.UpdatePatchProduct{
			Id:     updatePatchProduct.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteProduct godoc
// @ID delete_product
// @Router /product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProduct(c *gin.Context) {

	productId := c.Param("id")

	if !util.IsValidUUID(productId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().Delete(
		c.Request.Context(),
		&tovar_service.ProductPrimaryKey{Id: productId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
