package handlers

import (
	"app/api/http"
	"app/config"

	"github.com/gin-gonic/gin"
)

func (h *Handler) GetConfig(c *gin.Context) {

	switch h.cfg.Environment {
	case config.DebugMode:
		h.handleResponse(c, http.OK, h.cfg)
		return
	case config.TestMode:
		h.handleResponse(c, http.OK, h.cfg.Environment)
		return
	case config.ReleaseMode:
		h.handleResponse(c, http.OK, "private data")
		return
	}

	h.handleResponse(c, http.BadEnvironment, "wrong environment value passed")
}
