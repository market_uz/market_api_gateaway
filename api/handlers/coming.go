package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/sklad_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateComing godoc
// @ID create_coming
// @Router /coming [POST]
// @Summary Create Coming
// @Description  Create Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param profile body sklad_service.CreateComing true "CreateComingRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "GetComingBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateComing(c *gin.Context) {
	var coming sklad_service.CreateComing

	err := c.ShouldBindJSON(&coming)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingService().Create(
		c.Request.Context(),
		&coming,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetComingByID godoc
// @ID get_coming_by_id
// @Router /coming/{id} [GET]
// @Summary Get Coming By ID
// @Description Get Coming By ID
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "ComingBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingByID(c *gin.Context) {

	comingId := c.Param("id")

	if !util.IsValidUUID(comingId) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingService().GetByID(
		context.Background(),
		&sklad_service.ComingPrimaryKey{
			Id: comingId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetComingList godoc
// @ID get_coming_list
// @Router /coming [GET]
// @Summary Get Coming List
// @Description Get Coming List
// @Tags Coming
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=sklad_service.GetListComingResponse} "GetAllComingResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingService().GetList(
		context.Background(),
		&sklad_service.GetListComingRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_coming
// @Router /coming/{id} [PUT]
// @Summary Update Coming
// @Description Update Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sklad_service.UpdateComing true "UpdateComingRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "Coming data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateComing(c *gin.Context) {

	var coming sklad_service.UpdateComing

	coming.Id = c.Param("id")

	if !util.IsValidUUID(coming.Id) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&coming)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingService().Update(
		c.Request.Context(),
		&coming,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchComing godoc
// @ID patch_coming
// @Router /coming/{id} [PATCH]
// @Summary Patch Coming
// @Description Patch Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "Coming data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchComing(c *gin.Context) {

	var updatePatchcoming models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchcoming)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchcoming.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchcoming.ID) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchcoming.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingService().UpdatePatch(
		c.Request.Context(),
		&sklad_service.UpdatePatchComing{
			Id:     updatePatchcoming.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteComing godoc
// @ID delete_coming
// @Router /coming/{id} [DELETE]
// @Summary Delete Coming
// @Description Delete Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Coming data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteComing(c *gin.Context) {

	comingId := c.Param("id")

	if !util.IsValidUUID(comingId) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingService().Delete(
		c.Request.Context(),
		&sklad_service.ComingPrimaryKey{Id: comingId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
