package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/organization_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateShop godoc
// @ID create_shop
// @Router /shop [POST]
// @Summary Create Shop
// @Description  Create Shop
// @Tags Shop
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateShop true "CreateShopRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Shop} "GetShopBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShop(c *gin.Context) {
	var shop organization_service.CreateShop

	err := c.ShouldBindJSON(&shop)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopService().Create(
		c.Request.Context(),
		&shop,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetShopByID godoc
// @ID get_shop_by_id
// @Router /shop/{id} [GET]
// @Summary Get Shop By ID
// @Description Get Shop By ID
// @Tags Shop
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Shop} "ShopBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopByID(c *gin.Context) {

	shopId := c.Param("id")

	if !util.IsValidUUID(shopId) {
		h.handleResponse(c, http.InvalidArgument, "shop id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopService().GetByID(
		context.Background(),
		&organization_service.ShopPrimaryKey{
			Id: shopId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetShopList godoc
// @ID get_shop_list
// @Router /shop [GET]
// @Summary Get Shop List
// @Description Get Shop List
// @Tags Shop
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListShopResponse} "GetAllShopResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopService().GetList(
		context.Background(),
		&organization_service.GetListShopRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_shop
// @Router /shop/{id} [PUT]
// @Summary Update Shop
// @Description Update Shop
// @Tags Shop
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateShop true "UpdateShopRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Shop} "Shop data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShop(c *gin.Context) {

	var shop organization_service.UpdateShop

	shop.Id = c.Param("id")

	if !util.IsValidUUID(shop.Id) {
		h.handleResponse(c, http.InvalidArgument, "shop id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&shop)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopService().Update(
		c.Request.Context(),
		&shop,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchShop godoc
// @ID patch_shop
// @Router /shop/{id} [PATCH]
// @Summary Patch Shop
// @Description Patch Shop
// @Tags Shop
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Shop} "Shop data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchShop(c *gin.Context) {

	var updatePatchShop models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchShop)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchShop.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchShop.ID) {
		h.handleResponse(c, http.InvalidArgument, "shop id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchShop.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopService().UpdatePatch(
		c.Request.Context(),
		&organization_service.UpdatePatchShop{
			Id:     updatePatchShop.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteShop godoc
// @ID delete_shop
// @Router /shop/{id} [DELETE]
// @Summary Delete Shop
// @Description Delete Shop
// @Tags Shop
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shop data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShop(c *gin.Context) {

	shopId := c.Param("id")

	if !util.IsValidUUID(shopId) {
		h.handleResponse(c, http.InvalidArgument, "shop id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopService().Delete(
		c.Request.Context(),
		&organization_service.ShopPrimaryKey{Id: shopId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
