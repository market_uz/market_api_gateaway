package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/sklad_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateComingProduct godoc
// @ID create_coming
// @Router /coming-product [POST]
// @Summary Create ComingProduct
// @Description  Create ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param profile body sklad_service.CreateComingProduct true "CreateComingProductRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "GetComingProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateComingProduct(c *gin.Context) {
	var comingProduct sklad_service.CreateComingProduct

	err := c.ShouldBindJSON(&comingProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().Create(
		c.Request.Context(),
		&comingProduct,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetComingProductByID godoc
// @ID get_coming_product_by_id
// @Router /coming-product/{id} [GET]
// @Summary Get ComingProduct By ID
// @Description Get ComingProduct By ID
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sklad_service.ComingProduct} "ComingProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductByID(c *gin.Context) {

	comingProductId := c.Param("id")

	if !util.IsValidUUID(comingProductId) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingProductService().GetByID(
		context.Background(),
		&sklad_service.ComingProductPrimaryKey{
			Id: comingProductId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetComingProductList godoc
// @ID get_coming_product_list
// @Router /coming-product [GET]
// @Summary Get ComingProduct List
// @Description Get ComingProduct List
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=sklad_service.GetListComingProductResponse} "GetAllComingProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().GetList(
		context.Background(),
		&sklad_service.GetListComingProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_coming_product
// @Router /coming-product/{id} [PUT]
// @Summary Update ComingProduct
// @Description Update ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sklad_service.UpdateComingProduct true "UpdateComingProductRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.ComingProduct} "ComingProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateComingProduct(c *gin.Context) {

	var comingProduct sklad_service.UpdateComingProduct

	comingProduct.Id = c.Param("id")

	if !util.IsValidUUID(comingProduct.Id) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&comingProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().Update(
		c.Request.Context(),
		&comingProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchComingProduct godoc
// @ID patch_coming_product
// @Router /coming-product/{id} [PATCH]
// @Summary Patch ComingProduct
// @Description Patch ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Coming} "ComingProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchComingProduct(c *gin.Context) {

	var updatePatchComingProduct models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchComingProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchComingProduct.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchComingProduct.ID) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchComingProduct.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().UpdatePatch(
		c.Request.Context(),
		&sklad_service.UpdatePatchComingProduct{
			Id:     updatePatchComingProduct.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteComingProduct godoc
// @ID delete_coming_product
// @Router /coming-product/{id} [DELETE]
// @Summary Delete ComingProduct
// @Description Delete ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ComingProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteComingProduct(c *gin.Context) {

	comingProductId := c.Param("id")

	if !util.IsValidUUID(comingProductId) {
		h.handleResponse(c, http.InvalidArgument, "coming product id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingProductService().Delete(
		c.Request.Context(),
		&sklad_service.ComingProductPrimaryKey{Id: comingProductId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
