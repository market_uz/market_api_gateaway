package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateSmena godoc
// @ID create_smena
// @Router /smena [POST]
// @Summary Create Smena
// @Description  Create Smena
// @Tags Smena
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateSmena true "CreateSmenaRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Smena} "GetSmenaBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSmena(c *gin.Context) {
	var smena kassa_service.CreateSmena

	err := c.ShouldBindJSON(&smena)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().Create(
		c.Request.Context(),
		&smena,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSmenaByID godoc
// @ID get_smena_by_id
// @Router /smena/{id} [GET]
// @Summary Get Smena By ID
// @Description Get Smena By ID
// @Tags Smena
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Smena} "SmenaBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSmenaByID(c *gin.Context) {

	smenaID := c.Param("id")

	if !util.IsValidUUID(smenaID) {
		h.handleResponse(c, http.InvalidArgument, "smena id is an invalid uuid")
		return
	}

	resp, err := h.services.SmenaService().GetByID(
		context.Background(),
		&kassa_service.SmenaPrimaryKey{
			Id: smenaID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetSmenaList godoc
// @ID get_smena_list
// @Router /smena [GET]
// @Summary Get Smena List
// @Description Get Smena List
// @Tags Smena
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListSmenaResponse} "GetAllSmenaResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSmenaList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SmenaService().GetList(
		context.Background(),
		&kassa_service.GetListSmenaRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID updateSmena
// @Router /smena/{id} [PUT]
// @Summary Update Smena
// @Description Update Smena
// @Tags Smena
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSmena true "UpdateSmenaRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Smena} "Smena data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSmena(c *gin.Context) {

	var smena kassa_service.UpdateSmena

	smena.Id = c.Param("id")

	if !util.IsValidUUID(smena.Id) {
		h.handleResponse(c, http.InvalidArgument, "smena id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&smena)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SmenaService().Update(
		c.Request.Context(),
		&smena,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchSmena godoc
// @ID patch_smena
// @Router /smena/{id} [PATCH]
// @Summary Patch Smena
// @Description Patch Smena
// @Tags Smena
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Smena} "Smena data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchSmena(c *gin.Context) {

	var updatePatchSmena models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchSmena)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchSmena.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchSmena.ID) {
		h.handleResponse(c, http.InvalidArgument, "smena id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchSmena.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SmenaService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchSmena{
			Id:     updatePatchSmena.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSmena godoc
// @ID delete_smena
// @Router /smena/{id} [DELETE]
// @Summary Delete Smena
// @Description Delete Smena
// @Tags Smena
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Smena data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSmena(c *gin.Context) {

	smenaId := c.Param("id")

	if !util.IsValidUUID(smenaId) {
		h.handleResponse(c, http.InvalidArgument, "smena id is an invalid uuid")
		return
	}

	resp, err := h.services.SmenaService().Delete(
		c.Request.Context(),
		&kassa_service.SmenaPrimaryKey{Id: smenaId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
