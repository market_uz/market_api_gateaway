package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/organization_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateShopEmployee godoc
// @ID create_shop_employee
// @Router /shop-employee [POST]
// @Summary Create Shop Employee
// @Description  Create Shop Employee
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateShopEmployee true "CreateShopEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.ShopEmployee} "GetShopEmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShopEmployee(c *gin.Context) {
	var shopEmployee organization_service.CreateShopEmployee

	err := c.ShouldBindJSON(&shopEmployee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopEmployeeService().Create(
		c.Request.Context(),
		&shopEmployee,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetShopEmployeeByID godoc
// @ID get_shop_employee_by_id
// @Router /shop-employee/{id} [GET]
// @Summary Get ShopEmployee By ID
// @Description Get ShopEmployee By ID
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.ShopEmployee} "ShopEmployeeBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopEmployeeByID(c *gin.Context) {

	shopEmployeeId := c.Param("id")

	if !util.IsValidUUID(shopEmployeeId) {
		h.handleResponse(c, http.InvalidArgument, "shopEmployee id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopService().GetByID(
		context.Background(),
		&organization_service.ShopPrimaryKey{
			Id: shopEmployeeId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetShopEmployeeList godoc
// @ID get_shop_employee_list
// @Router /shop-employee [GET]
// @Summary Get ShopEmployee List
// @Description Get ShopEmployee List
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListShopEmployeeResponse} "GetAllShopEmployeeResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopEmployeeList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopEmployeeService().GetList(
		context.Background(),
		&organization_service.GetListShopEmployeeRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_shop_employee
// @Router /shop-employee/{id} [PUT]
// @Summary Update ShopEmployee
// @Description Update ShopEmployee
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateShopEmployee true "UpdateShopEmployeeRequestBody"
// @Success 200 {object} http.Response{data=organization_service.ShopEmployee} "ShopEmployee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShopEmployee(c *gin.Context) {

	var shopEmployee organization_service.UpdateShopEmployee

	shopEmployee.Id = c.Param("id")

	if !util.IsValidUUID(shopEmployee.Id) {
		h.handleResponse(c, http.InvalidArgument, "shopEmployee id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&shopEmployee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopEmployeeService().Update(
		c.Request.Context(),
		&shopEmployee,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchShopEmployee godoc
// @ID patch_shop_employee
// @Router /shop-employee/{id} [PATCH]
// @Summary Patch ShopEmployee
// @Description Patch ShopEmployee
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.ShopEmployee} "ShopEmployee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchShopEmployee(c *gin.Context) {

	var updatePatchShopEmployee models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchShopEmployee)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchShopEmployee.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchShopEmployee.ID) {
		h.handleResponse(c, http.InvalidArgument, "shopEmployee id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchShopEmployee.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopEmployeeService().UpdatePatch(
		c.Request.Context(),
		&organization_service.UpdatePatchShopEmployee{
			Id:     updatePatchShopEmployee.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteShopEmployee godoc
// @ID delete_shop_empliyee
// @Router /shop-employee/{id} [DELETE]
// @Summary Delete ShopEmployee
// @Description Delete ShopEmployee
// @Tags ShopEmployee
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ShopEmployee data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShopEmployee(c *gin.Context) {

	shopEmployeeId := c.Param("id")

	if !util.IsValidUUID(shopEmployeeId) {
		h.handleResponse(c, http.InvalidArgument, "shopEmployee id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopEmployeeService().Delete(
		c.Request.Context(),
		&organization_service.ShopEmployeePrimaryKey{Id: shopEmployeeId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
