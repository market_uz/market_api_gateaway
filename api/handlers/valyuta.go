package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateCurrency godoc
// @ID create_currency_from_api
// @Router /currency-api [POST]
// @Summary Create Currency
// @Description  Create Currency
// @Tags Currency
// @Accept json
// @Produce json
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) AddFromExternalApi(c *gin.Context) {
	_, err := h.services.ValyutaService().AddFromApi(context.Background(), &kassa_service.AddFromApiRequest{})
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, nil)
}

// CreateCurrency godoc
// @ID create_currency
// @Router /currency [POST]
// @Summary Create Currency
// @Description  Create Currency
// @Tags Currency
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateValyuta true "CreateValyutaRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Valyuta} "GetValyutaBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCurrency(c *gin.Context) {
	var Currency kassa_service.CreateValyuta

	err := c.ShouldBindJSON(&Currency)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ValyutaService().Create(
		c.Request.Context(),
		&Currency,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCurrencyByID godoc
// @ID get_currency_by_id
// @Router /currency/{id} [GET]
// @Summary Get Currency By ID
// @Description Get Currency By ID
// @Tags Currency
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Valyuta} "ValyutaBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCurrencyByID(c *gin.Context) {

	CurrencyID := c.Param("id")

	if !util.IsValidUUID(CurrencyID) {
		h.handleResponse(c, http.InvalidArgument, "Currency id is an invalid uuid")
		return
	}

	resp, err := h.services.ValyutaService().GetByID(
		context.Background(),
		&kassa_service.ValyutaPrimaryKey{
			Id: CurrencyID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetCurrencyList godoc
// @ID get_currency_list
// @Router /currency [GET]
// @Summary Get Currency List
// @Description Get Currency List
// @Tags Currency
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListValyutaResponse} "GetAllValyutaResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCurrencyList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ValyutaService().GetList(
		context.Background(),
		&kassa_service.GetListValyutaRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID updateCurrency
// @Router /currency/{id} [PUT]
// @Summary Update Currency
// @Description Update Currency
// @Tags Currency
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateValyuta true "UpdateValyutaRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Valyuta} "Currency data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCurrency(c *gin.Context) {

	var currency kassa_service.UpdateValyuta

	currency.Id = c.Param("id")

	if !util.IsValidUUID(currency.Id) {
		h.handleResponse(c, http.InvalidArgument, "Currency id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&currency)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ValyutaService().Update(
		c.Request.Context(),
		&currency,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchCurrency godoc
// @ID patch_currency
// @Router /currency/{id} [PATCH]
// @Summary Patch Currency
// @Description Patch Currency
// @Tags Currency
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Valyuta} "Valyuta data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchCurrency(c *gin.Context) {

	var updatePatchCurrency models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchCurrency)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchCurrency.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchCurrency.ID) {
		h.handleResponse(c, http.InvalidArgument, "Currency id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchCurrency.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ValyutaService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchValyuta{
			Id:     updatePatchCurrency.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCurrency godoc
// @ID delete_currency
// @Router /currency/{id} [DELETE]
// @Summary Delete Currency
// @Description Delete Currency
// @Tags Currency
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Valyuta data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCurrency(c *gin.Context) {

	CurrencyId := c.Param("id")

	if !util.IsValidUUID(CurrencyId) {
		h.handleResponse(c, http.InvalidArgument, "valyuta id is an invalid uuid")
		return
	}

	resp, err := h.services.ValyutaService().Delete(
		c.Request.Context(),
		&kassa_service.ValyutaPrimaryKey{Id: CurrencyId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
