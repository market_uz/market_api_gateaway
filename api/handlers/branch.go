package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/organization_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @ID create_branch
// @Router /branch [POST]
// @Summary Create Branch
// @Description  Create Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param profile body organization_service.CreateBranch true "CreateBranchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Branch} "GetBranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBranch(c *gin.Context) {
	var branch organization_service.CreateBranch

	err := c.ShouldBindJSON(&branch)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Create(
		c.Request.Context(),
		&branch,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetBranchByID godoc
// @ID get_branch_by_id
// @Router /branch/{id} [GET]
// @Summary Get Branch By ID
// @Description Get Branch By ID
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Branch} "BranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchByID(c *gin.Context) {

	branchId := c.Param("id")

	if !util.IsValidUUID(branchId) {
		h.handleResponse(c, http.InvalidArgument, "branch id is an invalid uuid")
		return
	}

	resp, err := h.services.BranchService().GetByID(
		context.Background(),
		&organization_service.BranchPrimaryKey{
			Id: branchId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetBranchList godoc
// @ID get_branch_list
// @Router /branch [GET]
// @Summary Get Branch List
// @Description Get Branch List
// @Tags Branch
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=organization_service.GetListBranchResponse} "GetAllBranchResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.BranchService().GetList(
		context.Background(),
		&organization_service.GetListBranchRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_branch
// @Router /branch/{id} [PUT]
// @Summary Update Branch
// @Description Update Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateBranch true "UpdateBranchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Branch} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBranch(c *gin.Context) {

	var branch organization_service.UpdateBranch

	branch.Id = c.Param("id")

	if !util.IsValidUUID(branch.Id) {
		h.handleResponse(c, http.InvalidArgument, "branch id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&branch)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Update(
		c.Request.Context(),
		&branch,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchCourier godoc
// @ID patch_branch
// @Router /branch/{id} [PATCH]
// @Summary Patch Branch
// @Description Patch Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Branch} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchBranch(c *gin.Context) {

	var updatePatchBranch models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchBranch)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchBranch.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchBranch.ID) {
		h.handleResponse(c, http.InvalidArgument, "branch id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchBranch.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.BranchService().UpdatePatch(
		c.Request.Context(),
		&organization_service.UpdatePatchBranch{
			Id:     updatePatchBranch.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteBranch godoc
// @ID delete_branch
// @Router /branch/{id} [DELETE]
// @Summary Delete Branch
// @Description Delete Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBranch(c *gin.Context) {

	branchId := c.Param("id")

	if !util.IsValidUUID(branchId) {
		h.handleResponse(c, http.InvalidArgument, "branch id is an invalid uuid")
		return
	}

	resp, err := h.services.BranchService().Delete(
		c.Request.Context(),
		&organization_service.BranchPrimaryKey{Id: branchId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
