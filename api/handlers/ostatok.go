package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/sklad_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateOstatok godoc
// @ID create_ostatok
// @Router /ostatok [POST]
// @Summary Create Ostatok
// @Description  Create Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param profile body sklad_service.CreateOstatok true "CreateOstatokRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Ostatok} "GetOstatokBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateOstatok(c *gin.Context) {
	var ostatok sklad_service.CreateOstatok

	err := c.ShouldBindJSON(&ostatok)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OstatokService().Create(
		c.Request.Context(),
		&ostatok,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetOstatokByID godoc
// @ID get_ostatok_by_id
// @Router /ostatok/{id} [GET]
// @Summary Get Ostatok By ID
// @Description Get Ostatok By ID
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=sklad_service.Ostatok} "OstatokBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOstatokByID(c *gin.Context) {

	ostatokId := c.Param("id")

	if !util.IsValidUUID(ostatokId) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	resp, err := h.services.OstatokService().GetByID(
		context.Background(),
		&sklad_service.OstatokPrimaryKey{
			Id: ostatokId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetOstatokList godoc
// @ID get_ostatok_list
// @Router /ostatok [GET]
// @Summary Get Ostatok List
// @Description Get Ostatok List
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=sklad_service.GetListOstatokResponse} "GetAllOstatokResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOstatokList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OstatokService().GetList(
		context.Background(),
		&sklad_service.GetListOstatokRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID update_ostatok
// @Router /ostatok/{id} [PUT]
// @Summary Update Ostatok
// @Description Update Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body sklad_service.UpdateOstatok true "UpdateOstatokRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Ostatok} "Ostatok data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateOstatok(c *gin.Context) {

	var ostatok sklad_service.UpdateOstatok

	ostatok.Id = c.Param("id")

	if !util.IsValidUUID(ostatok.Id) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&ostatok)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OstatokService().Update(
		c.Request.Context(),
		&ostatok,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchOstatok godoc
// @ID patch_ostatok
// @Router /ostatok/{id} [PATCH]
// @Summary Patch Ostatok
// @Description Patch Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=sklad_service.Ostatok} "Ostatok data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchOstatok(c *gin.Context) {

	var updatePatchOstatok models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchOstatok)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchOstatok.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchOstatok.ID) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchOstatok.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OstatokService().UpdatePatch(
		c.Request.Context(),
		&sklad_service.UpdatePatchOstatok{
			Id:     updatePatchOstatok.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteOstatok godoc
// @ID delete_ostatok
// @Router /ostatok/{id} [DELETE]
// @Summary Delete Ostatok
// @Description Delete Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Ostatok data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteOstatok(c *gin.Context) {

	ostatokId := c.Param("id")

	if !util.IsValidUUID(ostatokId) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	resp, err := h.services.OstatokService().Delete(
		c.Request.Context(),
		&sklad_service.OstatokPrimaryKey{Id: ostatokId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
