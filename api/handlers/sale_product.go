package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateSaleProduct godoc
// @ID create_sale_product
// @Router /sale-product [POST]
// @Summary Create SaleProduct
// @Description  Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateSaleProduct true "CreateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSaleProduct(c *gin.Context) {
	var saleProduct kassa_service.CreateSaleProduct

	err := c.ShouldBindJSON(&saleProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Create(
		c.Request.Context(),
		&saleProduct,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSaleProductByID godoc
// @ID get_sale_product_by_id
// @Router /sale-product/{id} [GET]
// @Summary Get SaleProduct By ID
// @Description Get SaleProduct By ID
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "SaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductByID(c *gin.Context) {

	saleProductId := c.Param("id")

	if !util.IsValidUUID(saleProductId) {
		h.handleResponse(c, http.InvalidArgument, "sale product id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductService().GetByID(
		context.Background(),
		&kassa_service.SaleProductPrimaryKey{
			Id: saleProductId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetSaleProductList godoc
// @ID get_sale_product_list
// @Router /sale-product [GET]
// @Summary Get SaleProduct List
// @Description Get SaleProduct List
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListSaleProductResponse} "GetAllSaleProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().GetList(
		context.Background(),
		&kassa_service.GetListSaleProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSaleProduct godoc
// @ID update_sale_product
// @Router /sale-product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSaleProduct true "UpdateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSaleProduct(c *gin.Context) {

	var saleProduct kassa_service.UpdateSaleProduct

	saleProduct.Id = c.Param("id")

	if !util.IsValidUUID(saleProduct.Id) {
		h.handleResponse(c, http.InvalidArgument, "sale product id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&saleProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Update(
		c.Request.Context(),
		&saleProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchSaleProduct godoc
// @ID patch_sale_product
// @Router /sale-product/{id} [PATCH]
// @Summary Patch SaleProduct
// @Description Patch SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchSaleProduct(c *gin.Context) {

	var updatePatchSaleProduct models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchSaleProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchSaleProduct.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchSaleProduct.ID) {
		h.handleResponse(c, http.InvalidArgument, "sale product id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchSaleProduct.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchSaleProduct{
			Id:     updatePatchSaleProduct.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSaleProduct godoc
// @ID delete_sale_product
// @Router /sale-product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {

	saleProductId := c.Param("id")

	if !util.IsValidUUID(saleProductId) {
		h.handleResponse(c, http.InvalidArgument, "sale product id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductService().Delete(
		c.Request.Context(),
		&kassa_service.SaleProductPrimaryKey{Id: saleProductId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
