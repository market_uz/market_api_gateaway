package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateTransaction godoc
// @ID create_transaction
// @Router /transaction [POST]
// @Summary Create Transaction
// @Description  Create Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateTransaction true "CreateTransactionRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "GetTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTransaction(c *gin.Context) {
	var transaction kassa_service.CreateTransaction

	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Create(
		c.Request.Context(),
		&transaction,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetTransactionByID godoc
// @ID get_transaction_by_id
// @Router /transaction/{id} [GET]
// @Summary Get Transaction By ID
// @Description Get Transaction By ID
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "TransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionByID(c *gin.Context) {

	transactionID := c.Param("id")

	if !util.IsValidUUID(transactionID) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().GetByID(
		context.Background(),
		&kassa_service.TransactionPrimaryKey{
			Id: transactionID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetTransactionList godoc
// @ID get_transaction_list
// @Router /transaction [GET]
// @Summary Get Transaction List
// @Description Get Transaction List
// @Tags Transaction
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListTransactionResponse} "GetAllTransactionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.TransactionService().GetList(
		context.Background(),
		&kassa_service.GetListTransactionRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @ID updateTransaction
// @Router /transaction/{id} [PUT]
// @Summary Update Transaction
// @Description Update Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateTransaction true "UpdateTransactionRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTransaction(c *gin.Context) {

	var transaction kassa_service.UpdateTransaction

	transaction.Id = c.Param("id")

	if !util.IsValidUUID(transaction.Id) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Update(
		c.Request.Context(),
		&transaction,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchTransaction godoc
// @ID patch_transaction
// @Router /transaction/{id} [PATCH]
// @Summary Patch Transaction
// @Description Patch Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchTransaction(c *gin.Context) {

	var updatePatchTransaction models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchTransaction)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchTransaction.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchTransaction.ID) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchTransaction.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.TransactionService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchTransaction{
			Id:     updatePatchTransaction.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteTransaction godoc
// @ID delete_transaction
// @Router /transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTransaction(c *gin.Context) {

	transactionId := c.Param("id")

	if !util.IsValidUUID(transactionId) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().Delete(
		c.Request.Context(),
		&kassa_service.TransactionPrimaryKey{Id: transactionId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
