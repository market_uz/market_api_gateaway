package handlers

import (
	"app/api/http"
	"app/api/models"
	"app/genproto/kassa_service"
	"app/pkg/helper"
	"app/pkg/util"
	"context"

	"github.com/gin-gonic/gin"
)

// CreateSale godoc
// @ID create_sale
// @Router /sale [POST]
// @Summary Create Sale
// @Description  Create Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param profile body kassa_service.CreateSale true "CreateSaleRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "GetSaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSale(c *gin.Context) {
	var sale kassa_service.CreateSale

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Create(
		c.Request.Context(),
		&sale,
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSaleByID godoc
// @ID get_sale_by_id
// @Router /sale/{id} [GET]
// @Summary Get Sale By ID
// @Description Get Sale By ID
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "SaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleByID(c *gin.Context) {

	saleId := c.Param("id")

	if !util.IsValidUUID(saleId) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().GetByID(
		context.Background(),
		&kassa_service.SalePrimaryKey{
			Id: saleId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetSaleList godoc
// @ID get_sale_list
// @Router /sale [GET]
// @Summary Get Sale List
// @Description Get Sale List
// @Tags Sale
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Success 200 {object} http.Response{data=kassa_service.GetListSaleResponse} "GetAllSaleResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleService().GetList(
		context.Background(),
		&kassa_service.GetListSaleRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSale godoc
// @ID update_sale
// @Router /sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSale true "UpdateSaleRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSale(c *gin.Context) {

	var sale kassa_service.UpdateSale

	sale.Id = c.Param("id")

	if !util.IsValidUUID(sale.Id) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Update(
		c.Request.Context(),
		&sale,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchSale godoc
// @ID patch_sale
// @Router /sale/{id} [PATCH]
// @Summary Patch Sale
// @Description Patch Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchSale(c *gin.Context) {

	var updatePatchSale models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchSale)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchSale.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchSale.ID) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchSale.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleService().UpdatePatch(
		c.Request.Context(),
		&kassa_service.UpdatePatchSale{
			Id:     updatePatchSale.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSale godoc
// @ID delete_sale
// @Router /sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSale(c *gin.Context) {

	saleId := c.Param("id")

	if !util.IsValidUUID(saleId) {
		h.handleResponse(c, http.InvalidArgument, "Sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().Delete(
		c.Request.Context(),
		&kassa_service.SalePrimaryKey{Id: saleId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
