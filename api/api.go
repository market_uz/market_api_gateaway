package api

import (
	_ "app/api/docs"
	"app/api/handlers"
	"app/config"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {

	//r.GET("/config", h.GetConfig)

	// PRODUCT
	r.POST("/product", h.CreateProduct)
	r.GET("/product", h.GetProductList)
	r.GET("/product/:id", h.GetProductByID)
	r.PUT("/product/:id", h.UpdateProduct)
	r.PATCH("/product/:id", h.UpdatePatchProduct)
	r.DELETE("/product/:id", h.DeleteProduct)

	//CATEGORY
	r.POST("/category", h.CreateCategory)
	r.GET("/category", h.GetCategoryList)
	r.GET("/category/:id", h.GetCategoryByID)
	r.PUT("/category/:id", h.UpdateCategory)
	r.PATCH("/category/:id", h.UpdatePatchCategory)
	r.DELETE("/category/:id", h.DeleteCategory)

	// BRANCH
	r.POST("/branch", h.CreateBranch)
	r.GET("/branch", h.GetBranchList)
	r.GET("/branch/:id", h.GetBranchByID)
	r.PUT("/branch/:id", h.UpdateBranch)
	r.PATCH("/branch/:id", h.UpdatePatchBranch)
	r.DELETE("/branch/:id", h.DeleteBranch)

	// COURIER
	r.POST("/courier", h.CreateCourier)
	r.GET("/courier", h.GetCourierList)
	r.GET("/courier/:id", h.GetCourierByID)
	r.PUT("/courier/:id", h.UpdateCourier)
	r.PATCH("/courier/:id", h.UpdatePatchCourier)
	r.DELETE("/courier/:id", h.DeleteCourier)

	// EMPLOYEE
	r.POST("/employee", h.CreateEmployee)
	r.GET("/employee", h.GetEmployeeList)
	r.GET("/employee/:id", h.GetEmployeeByID)
	r.PUT("/employee/:id", h.UpdateEmployee)
	r.PATCH("/employee/:id", h.UpdatePatchEmployee)
	r.DELETE("/employee/:id", h.DeleteEmployee)

	// SHOP
	r.POST("/shop", h.CreateShop)
	r.GET("/shop", h.GetShopList)
	r.GET("/shop/:id", h.GetShopByID)
	r.PUT("/shop/:id", h.UpdateShop)
	r.PATCH("/shop/:id", h.UpdatePatchShop)
	r.DELETE("/shop/:id", h.DeleteShop)

	// SHOP_EMPLOYEE
	r.POST("/shop-employee", h.CreateShopEmployee)
	r.GET("/shop-employee", h.GetShopEmployeeList)
	r.GET("/shop-employee/:id", h.GetShopEmployeeByID)
	r.PUT("/shop-employee/:id", h.UpdateShopEmployee)
	r.PATCH("/shop-employee/:id", h.UpdatePatchShopEmployee)
	r.DELETE("/shop-employee/:id", h.DeleteShopEmployee)

	// COMING
	r.POST("/coming", h.CreateComing)
	r.GET("/coming", h.GetComingList)
	r.GET("/coming/:id", h.GetComingByID)
	r.PUT("/coming/:id", h.UpdateComing)
	r.PATCH("/coming/:id", h.UpdatePatchComing)
	r.DELETE("/coming/:id", h.DeleteComing)

	// OSTATOK
	r.POST("/ostatok", h.CreateOstatok)
	r.GET("/ostatok", h.GetOstatokList)
	r.GET("/ostatok/:id", h.GetOstatokByID)
	r.PUT("/ostatok/:id", h.UpdateOstatok)
	r.PATCH("/ostatok/:id", h.UpdatePatchOstatok)
	r.DELETE("/ostatok/:id", h.DeleteOstatok)

	// COMING-PRODUCT
	r.POST("/coming-product", h.CreateComingProduct)
	r.GET("/coming-product", h.GetComingProductList)
	r.GET("/coming-product/:id", h.GetComingProductByID)
	r.PUT("/coming-product/:id", h.UpdateComingProduct)
	r.PATCH("/coming-product/:id", h.UpdatePatchComingProduct)
	r.DELETE("/coming-product/:id", h.DeleteComingProduct)

	// SALE
	r.POST("/sale", h.CreateSale)
	r.GET("/sale", h.GetSaleList)
	r.GET("/sale/:id", h.GetSaleByID)
	r.PUT("/sale/:id", h.UpdateSale)
	r.PATCH("/sale/:id", h.UpdatePatchSale)
	r.DELETE("/sale/:id", h.DeleteSale)

	// SALE-PRODUCT
	r.POST("/sale-product", h.CreateSaleProduct)
	r.GET("/sale-product", h.GetSaleProductList)
	r.GET("/sale-product/:id", h.GetSaleProductByID)
	r.PUT("/sale-product/:id", h.UpdateSaleProduct)
	r.PATCH("/sale-product/:id", h.UpdatePatchSaleProduct)
	r.DELETE("/sale-product/:id", h.DeleteSaleProduct)

	// SALE-PAYMENT
	r.POST("/sale-payment", h.CreateSalePayment)
	r.GET("/sale-payment", h.GetSalePaymentList)
	r.GET("/sale-payment/:id", h.GetSalePaymentByID)
	r.PUT("/sale-payment/:id", h.UpdateSalePayment)
	r.PATCH("/sale-payment/:id", h.UpdatePatchSalePayment)
	r.DELETE("/sale-payment/:id", h.DeleteSalePayment)

	// SMENA
	r.POST("/smena", h.CreateSmena)
	r.GET("/smena", h.GetSmenaList)
	r.GET("/smena/:id", h.GetSmenaByID)
	r.PUT("/smena/:id", h.UpdateSmena)
	r.PATCH("/smena/:id", h.UpdatePatchSmena)
	r.DELETE("/smena/:id", h.DeleteSmena)

	// TRANSACTION
	r.POST("/transaction", h.CreateTransaction)
	r.GET("/transaction", h.GetTransactionList)
	r.GET("/transaction/:id", h.GetTransactionByID)
	r.PUT("/transaction/:id", h.UpdateTransaction)
	r.PATCH("/transaction/:id", h.UpdatePatchTransaction)
	r.DELETE("/transaction/:id", h.DeleteTransaction)

	// CURRENCY
	r.POST("/currency-api", h.AddFromExternalApi)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}
