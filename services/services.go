package services

import (
	"app/config"
	"app/genproto/kassa_service"
	"app/genproto/organization_service"
	"app/genproto/sklad_service"
	"app/genproto/tovar_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// tovar_service
	CategoryService() tovar_service.CategoryServiceClient
	ProductService() tovar_service.ProductServiceClient

	// organization service
	BranchService() organization_service.BranchServiceClient
	CourierService() organization_service.CourierServiceClient
	EmployeeService() organization_service.EmployeeServiceClient
	ShopService() organization_service.ShopServiceClient
	ShopEmployeeService() organization_service.ShopEmployeeServiceClient

	// sklad service
	ComingService() sklad_service.ComingServiceClient
	ComingProductService() sklad_service.ComingProductServiceClient
	OstatokService() sklad_service.OstatokServiceClient

	// kassa service
	SaleService() kassa_service.SaleServiceClient
	SaleProductService() kassa_service.SaleProductServiceClient
	SalePaymentService() kassa_service.SalePaymentServiceClient
	SmenaService() kassa_service.SmenaServiceClient
	TransactionService() kassa_service.TransactionServiceClient
	ValyutaService() kassa_service.ValyutaServiceClient
}

type grpcClients struct {
	// tovar service
	categoryService tovar_service.CategoryServiceClient
	productService  tovar_service.ProductServiceClient

	// organization service
	branchService       organization_service.BranchServiceClient
	courierService      organization_service.CourierServiceClient
	employeeService     organization_service.EmployeeServiceClient
	shopService         organization_service.ShopServiceClient
	shopEmployeeService organization_service.ShopEmployeeServiceClient

	// sklad service
	comingService        sklad_service.ComingServiceClient
	comingProductService sklad_service.ComingProductServiceClient
	ostatokService       sklad_service.OstatokServiceClient

	// kassa service
	saleService        kassa_service.SaleServiceClient
	saleProductService kassa_service.SaleProductServiceClient
	salePaymentService kassa_service.SalePaymentServiceClient
	smenaService       kassa_service.SmenaServiceClient
	transactionService kassa_service.TransactionServiceClient
	valyutaService     kassa_service.ValyutaServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Tovar Service
	connTovarService, err := grpc.Dial(
		cfg.TovarServiceHost+cfg.TovarGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Organization Service
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Sklad Service
	connSkladService, err := grpc.Dial(
		cfg.SkladServiceHost+cfg.SkladGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Kassa Service
	connKassaService, err := grpc.Dial(
		cfg.KassaServiceHost+cfg.KassaGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// tovar service
		categoryService: tovar_service.NewCategoryServiceClient(connTovarService),
		productService:  tovar_service.NewProductServiceClient(connTovarService),

		// organization service
		branchService:       organization_service.NewBranchServiceClient(connOrganizationService),
		courierService:      organization_service.NewCourierServiceClient(connOrganizationService),
		employeeService:     organization_service.NewEmployeeServiceClient(connOrganizationService),
		shopService:         organization_service.NewShopServiceClient(connOrganizationService),
		shopEmployeeService: organization_service.NewShopEmployeeServiceClient(connOrganizationService),

		// sklad service
		comingService:        sklad_service.NewComingServiceClient(connSkladService),
		comingProductService: sklad_service.NewComingProductServiceClient(connSkladService),
		ostatokService:       sklad_service.NewOstatokServiceClient(connSkladService),

		// kassa service
		saleService:        kassa_service.NewSaleServiceClient(connKassaService),
		saleProductService: kassa_service.NewSaleProductServiceClient(connKassaService),
		salePaymentService: kassa_service.NewSalePaymentServiceClient(connKassaService),
		smenaService:       kassa_service.NewSmenaServiceClient(connKassaService),
		transactionService: kassa_service.NewTransactionServiceClient(connKassaService),
		valyutaService:     kassa_service.NewValyutaServiceClient(connKassaService),
	}, nil
}

func (g *grpcClients) CategoryService() tovar_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() tovar_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) CourierService() organization_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) EmployeeService() organization_service.EmployeeServiceClient {
	return g.employeeService
}

func (g *grpcClients) ShopService() organization_service.ShopServiceClient {
	return g.shopService
}

func (g *grpcClients) ShopEmployeeService() organization_service.ShopEmployeeServiceClient {
	return g.shopEmployeeService
}

func (g *grpcClients) ComingService() sklad_service.ComingServiceClient {
	return g.comingService
}

func (g *grpcClients) ComingProductService() sklad_service.ComingProductServiceClient {
	return g.comingProductService
}

func (g *grpcClients) OstatokService() sklad_service.OstatokServiceClient {
	return g.ostatokService
}

func (g *grpcClients) SaleService() kassa_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() kassa_service.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) SalePaymentService() kassa_service.SalePaymentServiceClient {
	return g.salePaymentService
}

func (g *grpcClients) ValyutaService() kassa_service.ValyutaServiceClient {
	return g.valyutaService
}

func (g *grpcClients) SmenaService() kassa_service.SmenaServiceClient {
	return g.smenaService
}

func (g *grpcClients) TransactionService() kassa_service.TransactionServiceClient {
	return g.transactionService
}
