package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string
	Domain      string

	DefaultOffset string
	DefaultLimit  string

	TovarServiceHost string
	TovarGRPCPort    string

	OrganizationServiceHost string
	OrganizationGRPCPort    string

	SkladServiceHost string
	SkladGRPCPort    string

	KassaServiceHost string
	KassaGRPCPort    string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("/go-api-gateway.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "market_go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	config.TovarServiceHost = cast.ToString(getOrReturnDefaultValue("TOVAR_SERVICE_HOST", "localhost"))
	config.TovarGRPCPort = cast.ToString(getOrReturnDefaultValue("TOVAR_GRPC_PORT", ":9101"))

	config.OrganizationServiceHost = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_SERVICE_HOST", "localhost"))
	config.OrganizationGRPCPort = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_GRPC_PORT", ":9102"))

	config.SkladServiceHost = cast.ToString(getOrReturnDefaultValue("SKLAD_SERVICE_HOST", "localhost"))
	config.SkladGRPCPort = cast.ToString(getOrReturnDefaultValue("SKLAD_GRPC_PORT", ":9103"))

	config.KassaServiceHost = cast.ToString(getOrReturnDefaultValue("KASSA_SERVICE_HOST", "localhost"))
	config.KassaGRPCPort = cast.ToString(getOrReturnDefaultValue("KASSA_GRPC_PORT", ":9104"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
