// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: smena.proto

package kassa_service

import (
	_struct "github.com/golang/protobuf/ptypes/struct"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SmenaPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
}

func (x *SmenaPrimaryKey) Reset() {
	*x = SmenaPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SmenaPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SmenaPrimaryKey) ProtoMessage() {}

func (x *SmenaPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SmenaPrimaryKey.ProtoReflect.Descriptor instead.
func (*SmenaPrimaryKey) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{0}
}

func (x *SmenaPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type Smena struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	IdNomer    string `protobuf:"bytes,2,opt,name=id_nomer,json=idNomer,proto3" json:"id_nomer"`
	EmployeeId string `protobuf:"bytes,3,opt,name=employee_id,json=employeeId,proto3" json:"employee_id"`
	BranchId   string `protobuf:"bytes,4,opt,name=branch_id,json=branchId,proto3" json:"branch_id"`
	ShopId     string `protobuf:"bytes,5,opt,name=shop_id,json=shopId,proto3" json:"shop_id"`
	Status     int32  `protobuf:"varint,6,opt,name=status,proto3" json:"status"`
	CreatedAt  string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at"`
	UpdatedAt  string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at"`
}

func (x *Smena) Reset() {
	*x = Smena{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Smena) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Smena) ProtoMessage() {}

func (x *Smena) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Smena.ProtoReflect.Descriptor instead.
func (*Smena) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{1}
}

func (x *Smena) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Smena) GetIdNomer() string {
	if x != nil {
		return x.IdNomer
	}
	return ""
}

func (x *Smena) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *Smena) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *Smena) GetShopId() string {
	if x != nil {
		return x.ShopId
	}
	return ""
}

func (x *Smena) GetStatus() int32 {
	if x != nil {
		return x.Status
	}
	return 0
}

func (x *Smena) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Smena) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateSmena struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	IdNomer    string `protobuf:"bytes,1,opt,name=id_nomer,json=idNomer,proto3" json:"id_nomer"`
	EmployeeId string `protobuf:"bytes,2,opt,name=employee_id,json=employeeId,proto3" json:"employee_id"`
	BranchId   string `protobuf:"bytes,3,opt,name=branch_id,json=branchId,proto3" json:"branch_id"`
	ShopId     string `protobuf:"bytes,4,opt,name=shop_id,json=shopId,proto3" json:"shop_id"`
}

func (x *CreateSmena) Reset() {
	*x = CreateSmena{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateSmena) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateSmena) ProtoMessage() {}

func (x *CreateSmena) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateSmena.ProtoReflect.Descriptor instead.
func (*CreateSmena) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{2}
}

func (x *CreateSmena) GetIdNomer() string {
	if x != nil {
		return x.IdNomer
	}
	return ""
}

func (x *CreateSmena) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *CreateSmena) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *CreateSmena) GetShopId() string {
	if x != nil {
		return x.ShopId
	}
	return ""
}

type UpdateSmena struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	IdNomer    string `protobuf:"bytes,2,opt,name=id_nomer,json=idNomer,proto3" json:"id_nomer"`
	EmployeeId string `protobuf:"bytes,3,opt,name=employee_id,json=employeeId,proto3" json:"employee_id"`
	BranchId   string `protobuf:"bytes,4,opt,name=branch_id,json=branchId,proto3" json:"branch_id"`
	ShopId     string `protobuf:"bytes,5,opt,name=shop_id,json=shopId,proto3" json:"shop_id"`
	Status     int32  `protobuf:"varint,6,opt,name=status,proto3" json:"status"`
}

func (x *UpdateSmena) Reset() {
	*x = UpdateSmena{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateSmena) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateSmena) ProtoMessage() {}

func (x *UpdateSmena) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateSmena.ProtoReflect.Descriptor instead.
func (*UpdateSmena) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateSmena) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateSmena) GetIdNomer() string {
	if x != nil {
		return x.IdNomer
	}
	return ""
}

func (x *UpdateSmena) GetEmployeeId() string {
	if x != nil {
		return x.EmployeeId
	}
	return ""
}

func (x *UpdateSmena) GetBranchId() string {
	if x != nil {
		return x.BranchId
	}
	return ""
}

func (x *UpdateSmena) GetShopId() string {
	if x != nil {
		return x.ShopId
	}
	return ""
}

func (x *UpdateSmena) GetStatus() int32 {
	if x != nil {
		return x.Status
	}
	return 0
}

type UpdatePatchSmena struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id     string          `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Fields *_struct.Struct `protobuf:"bytes,2,opt,name=fields,proto3" json:"fields"`
}

func (x *UpdatePatchSmena) Reset() {
	*x = UpdatePatchSmena{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePatchSmena) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePatchSmena) ProtoMessage() {}

func (x *UpdatePatchSmena) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePatchSmena.ProtoReflect.Descriptor instead.
func (*UpdatePatchSmena) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{4}
}

func (x *UpdatePatchSmena) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdatePatchSmena) GetFields() *_struct.Struct {
	if x != nil {
		return x.Fields
	}
	return nil
}

type GetListSmenaRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64 `protobuf:"varint,1,opt,name=offset,proto3" json:"offset"`
	Limit  int64 `protobuf:"varint,2,opt,name=limit,proto3" json:"limit"`
}

func (x *GetListSmenaRequest) Reset() {
	*x = GetListSmenaRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListSmenaRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListSmenaRequest) ProtoMessage() {}

func (x *GetListSmenaRequest) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListSmenaRequest.ProtoReflect.Descriptor instead.
func (*GetListSmenaRequest) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{5}
}

func (x *GetListSmenaRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListSmenaRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

type GetListSmenaResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count  int64    `protobuf:"varint,1,opt,name=count,proto3" json:"count"`
	Smenas []*Smena `protobuf:"bytes,2,rep,name=smenas,proto3" json:"smenas"`
}

func (x *GetListSmenaResponse) Reset() {
	*x = GetListSmenaResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_smena_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListSmenaResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListSmenaResponse) ProtoMessage() {}

func (x *GetListSmenaResponse) ProtoReflect() protoreflect.Message {
	mi := &file_smena_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListSmenaResponse.ProtoReflect.Descriptor instead.
func (*GetListSmenaResponse) Descriptor() ([]byte, []int) {
	return file_smena_proto_rawDescGZIP(), []int{6}
}

func (x *GetListSmenaResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListSmenaResponse) GetSmenas() []*Smena {
	if x != nil {
		return x.Smenas
	}
	return nil
}

var File_smena_proto protoreflect.FileDescriptor

var file_smena_proto_rawDesc = []byte{
	0x0a, 0x0b, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0d, 0x6b,
	0x61, 0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1c, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x73, 0x74,
	0x72, 0x75, 0x63, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x21, 0x0a, 0x0f, 0x53, 0x6d,
	0x65, 0x6e, 0x61, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0xdf, 0x01,
	0x0a, 0x05, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x69, 0x64, 0x5f, 0x6e, 0x6f,
	0x6d, 0x65, 0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x69, 0x64, 0x4e, 0x6f, 0x6d,
	0x65, 0x72, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x5f, 0x69,
	0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65,
	0x65, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64,
	0x12, 0x17, 0x0a, 0x07, 0x73, 0x68, 0x6f, 0x70, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x06, 0x73, 0x68, 0x6f, 0x70, 0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22,
	0x7f, 0x0a, 0x0b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x12, 0x19,
	0x0a, 0x08, 0x69, 0x64, 0x5f, 0x6e, 0x6f, 0x6d, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x69, 0x64, 0x4e, 0x6f, 0x6d, 0x65, 0x72, 0x12, 0x1f, 0x0a, 0x0b, 0x65, 0x6d, 0x70,
	0x6c, 0x6f, 0x79, 0x65, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a,
	0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72,
	0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x62,
	0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x68, 0x6f, 0x70, 0x5f,
	0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x68, 0x6f, 0x70, 0x49, 0x64,
	0x22, 0xa7, 0x01, 0x0a, 0x0b, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x53, 0x6d, 0x65, 0x6e, 0x61,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x19, 0x0a, 0x08, 0x69, 0x64, 0x5f, 0x6e, 0x6f, 0x6d, 0x65, 0x72, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x69, 0x64, 0x4e, 0x6f, 0x6d, 0x65, 0x72, 0x12, 0x1f, 0x0a, 0x0b, 0x65,
	0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0a, 0x65, 0x6d, 0x70, 0x6c, 0x6f, 0x79, 0x65, 0x65, 0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09,
	0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x68, 0x6f,
	0x70, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x68, 0x6f, 0x70,
	0x49, 0x64, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x53, 0x0a, 0x10, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x74, 0x63, 0x68, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x2f,
	0x0a, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17,
	0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66,
	0x2e, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74, 0x52, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x22,
	0x43, 0x0a, 0x13, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14,
	0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c,
	0x69, 0x6d, 0x69, 0x74, 0x22, 0x5a, 0x0a, 0x14, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x53,
	0x6d, 0x65, 0x6e, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05,
	0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75,
	0x6e, 0x74, 0x12, 0x2c, 0x0a, 0x06, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x73, 0x18, 0x02, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x14, 0x2e, 0x6b, 0x61, 0x73, 0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x2e, 0x53, 0x6d, 0x65, 0x6e, 0x61, 0x52, 0x06, 0x73, 0x6d, 0x65, 0x6e, 0x61, 0x73,
	0x42, 0x18, 0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6b, 0x61, 0x73,
	0x73, 0x61, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_smena_proto_rawDescOnce sync.Once
	file_smena_proto_rawDescData = file_smena_proto_rawDesc
)

func file_smena_proto_rawDescGZIP() []byte {
	file_smena_proto_rawDescOnce.Do(func() {
		file_smena_proto_rawDescData = protoimpl.X.CompressGZIP(file_smena_proto_rawDescData)
	})
	return file_smena_proto_rawDescData
}

var file_smena_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_smena_proto_goTypes = []interface{}{
	(*SmenaPrimaryKey)(nil),      // 0: kassa_service.SmenaPrimaryKey
	(*Smena)(nil),                // 1: kassa_service.Smena
	(*CreateSmena)(nil),          // 2: kassa_service.CreateSmena
	(*UpdateSmena)(nil),          // 3: kassa_service.UpdateSmena
	(*UpdatePatchSmena)(nil),     // 4: kassa_service.UpdatePatchSmena
	(*GetListSmenaRequest)(nil),  // 5: kassa_service.GetListSmenaRequest
	(*GetListSmenaResponse)(nil), // 6: kassa_service.GetListSmenaResponse
	(*_struct.Struct)(nil),       // 7: google.protobuf.Struct
}
var file_smena_proto_depIdxs = []int32{
	7, // 0: kassa_service.UpdatePatchSmena.fields:type_name -> google.protobuf.Struct
	1, // 1: kassa_service.GetListSmenaResponse.smenas:type_name -> kassa_service.Smena
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_smena_proto_init() }
func file_smena_proto_init() {
	if File_smena_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_smena_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SmenaPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Smena); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateSmena); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateSmena); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePatchSmena); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListSmenaRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_smena_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListSmenaResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_smena_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_smena_proto_goTypes,
		DependencyIndexes: file_smena_proto_depIdxs,
		MessageInfos:      file_smena_proto_msgTypes,
	}.Build()
	File_smena_proto = out.File
	file_smena_proto_rawDesc = nil
	file_smena_proto_goTypes = nil
	file_smena_proto_depIdxs = nil
}
